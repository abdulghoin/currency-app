import React from "react";

// redux
import { compose } from "redux";
import { connect } from "react-redux";

// module UI Components
import { List } from "@material-ui/core";

// Created UI Components
import CurrencyListItem from "./CurrencyListItem";

// styles
import style from "../assets/jss/components/CurrencyList";
import { withStyles } from "@material-ui/core/styles";

const CurrencyList = ({
  classes,
  currency: {
    data: { base, value, shows }
  }
}) =>
  shows.length > 0 ? (
    <List id='shown-currency-list' classes={{ padding: classes.listPadding }}>
      {shows.map(item => (
        <CurrencyListItem
          {...{
            key: item.currency_code,
            ...{ ...item, base_code: base, base_value: value }
          }}
        />
      ))}
    </List>
  ) : null;

CurrencyList.defaultProps = {
  currency: {
    data: {
      base: "",
      value: 0,
      shows: []
    }
  }
};

export default compose(
  withStyles(style),
  connect(({ currency }) => ({ currency }))
)(CurrencyList);
