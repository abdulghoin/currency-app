import React from "react";

// redux
import { compose } from "redux";
import { connect } from "react-redux";

// actions
import { updateBaseValue } from "../actions/currency";

// module UI Components
import { Divider } from "@material-ui/core";
import CurrencyFormat from "react-currency-format";

// styles
import style from "../assets/jss/components/HeaderInput";
import { withStyles } from "@material-ui/core/styles";

const HeaderInput = ({ classes, base, base_name, value, updateBaseValue }) => {
  return (
    <div className={classes.container}>
      <div className={classes.content}>
        <h5 className={classes.headerText}>
          {base} - {base_name}
        </h5>
        <div className={classes.inputContainer}>
          <span>{base}</span>
          <Divider className={classes.divider} />
          <CurrencyFormat
            className={classes.input}
            value={value}
            displayType="input"
            thousandSeparator={true}
            onValueChange={({ floatValue }) => updateBaseValue(floatValue)}
          />
        </div>
      </div>
    </div>
  );
};

HeaderInput.defaultProps = {
  base: "",
  base_name: "",
  value: 0
};

export default compose(
  withStyles(style),
  connect(
    ({
      currency: {
        data: { base, base_name, value }
      }
    }) => ({ base, base_name, value }),
    { updateBaseValue }
  )
)(HeaderInput);
