import React from "react";

// redux
import { compose } from "redux";
import { connect } from "react-redux";

// actions
import { updateModal } from "../actions/app";

// module UI Components
import { Fab } from "@material-ui/core";
import { Add } from "@material-ui/icons";

// created UI Components
import AddModal from "./AddModal";

// styles
import style from "../assets/jss/components/AddButton";
import { withStyles } from "@material-ui/core/styles";

const AddButton = ({ classes, updateModal }) => (
  <Fab
    size="medium"
    className={classes.fab}
    onClick={() => updateModal(<AddModal />)}
  >
    <Add />
  </Fab>
);

export default compose(
  withStyles(style),
  connect(
    null,
    { updateModal }
  )
)(AddButton);
