import React, { useState } from "react";

// redux
import { compose } from "redux";
import { connect } from "react-redux";

// actions
import { updateModal } from "../actions/app";
import { updateAvailableCurrencies } from "../actions/currency";

// module UI Components
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Select,
  OutlinedInput,
  MenuItem
} from "@material-ui/core";

// styles
import style from "../assets/jss/components/AddModal";
import { withStyles } from "@material-ui/core/styles";

const AddModal = ({
  rates,
  base,
  updateModal,
  updateAvailableCurrencies,
  classes
}) => {
  const [selected, setSelected] = useState("none");

  return (
    <Dialog
      {...{
        // fullScreen: true,
        // overrride class component class style
        classes: { paper: classes.dialogWidth },
        open: true,
        onClose: () => updateModal(null),
        "aria-labelledby": "responsive-dialog-title"
      }}
    >
      <DialogTitle id="responsive-dialog-title">Add Currency</DialogTitle>
      <DialogContent>
        <Select
          className={classes.selectWidth}
          onChange={e => setSelected(e.target.value)}
          value={selected}
          input={
            <OutlinedInput
              labelWidth={0}
              name="add-currency"
              id="add-currency"
            />
          }
        >
          <MenuItem key="none" value="none">
            <em>None</em>
          </MenuItem>
          {rates.map(({ show, currency_code, currency }) =>
            currency_code !== base && !show ? (
              <MenuItem key={currency_code} value={currency_code}>
                {currency_code} - {currency}
              </MenuItem>
            ) : null
          )}
        </Select>
      </DialogContent>
      <DialogActions>
        <Button
          className={classes.cancelButton}
          onClick={() => updateModal(null)}
        >
          Cancel
        </Button>
        <Button
          className={classes.nextButton}
          onClick={() => {
            // close modal
            updateModal(null);
            // update shown currencies
            updateAvailableCurrencies(selected);
            // scroll to top
            let listDom = document.getElementById('shown-currency-list');
            listDom.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
          }}
          disabled={selected === "none"}
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default compose(
  withStyles(style),
  connect(
    ({
      currency: {
        data: { rates, base }
      }
    }) => ({ rates, base }),
    { updateModal, updateAvailableCurrencies }
  )
)(AddModal);
