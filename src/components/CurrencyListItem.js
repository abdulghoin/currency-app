import React from "react";

// redux
import { compose } from "redux";
import { connect } from "react-redux";

// actions
import { updateAvailableCurrencies } from "../actions/currency";

// module UI Components
import {
  Grow,
  ListItem,
  ListItemSecondaryAction,
  IconButton
} from "@material-ui/core";
import { Delete } from "@material-ui/icons";
import CurrencyFormat from "react-currency-format";

// styles
import style from "../assets/jss/components/CurrencyListItem";
import { withStyles } from "@material-ui/core/styles";

const CurrencyListItem = ({
  currency_code,
  currency,
  value,
  base_code,
  base_value,
  classes,
  updateAvailableCurrencies
}) => (
  <Grow in={true}>
    <ListItem className='currency-list-item' classes={{ container: classes.container }}>
      <div className={classes.textContainer}>
        <div className={classes.topSection}>
          <span>{currency_code}</span>
          <CurrencyFormat
            value={(base_value * value).toFixed(2)}
            displayType="text"
            thousandSeparator={true}
          />
        </div>
        <div className={classes.bottomSection}>
          <p>
            {currency_code} - {currency}
          </p>
          <p>
            1 {base_code} =
            <CurrencyFormat
              prefix={` ${currency_code} `}
              value={value.toFixed(2)}
              displayType="text"
              thousandSeparator={true}
            />
          </p>
        </div>
      </div>
      <ListItemSecondaryAction>
        <IconButton
          onClick={() => updateAvailableCurrencies(currency_code)}
          className={classes.scdAction}
        >
          <Delete />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  </Grow>
);

CurrencyListItem.defaultProps = {
  currency_code: "",
  currency: "",
  value: 0,
  base_code: "",
  base_value: 0
};

export default compose(
  withStyles(style),
  connect(
    null,
    { updateAvailableCurrencies }
  )
)(CurrencyListItem);
