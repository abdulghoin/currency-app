import React, { useEffect, useState } from "react";

// redux
import { compose } from "redux";
import { connect } from "react-redux";

// actions
import { fetchCurrency } from "./actions/currency";

// created UI Components
import HeaderInput from "./components/HeaderInput";
import CurrencyList from "./components/CurrencyList";
import AddButton from "./components/AddButton";

// styles
import style from "./assets/jss/App";
import { withStyles } from "@material-ui/core/styles";

const App = ({ classes, modal, fetchCurrency }) => {
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    // get data right after component rendered / DidMount
    if (!isMounted) {
      fetchCurrency("USD", 10);
      setIsMounted(true);
    }
  });

  return (
    <section className={classes.container}>
      <HeaderInput />
      <CurrencyList />
      <AddButton />

      {/* app modal at the top container */}
      {modal}
    </section>
  );
};

export default compose(
  withStyles(style),
  connect(
    ({ app: { modal } }) => ({ modal }),
    { fetchCurrency }
  )
)(App);
