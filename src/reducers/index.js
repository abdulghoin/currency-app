import { combineReducers } from "redux";

// reducers
import app from "./appReducer";
import currency from "./currencyReducer";

export default combineReducers({
  app,
  currency
});
