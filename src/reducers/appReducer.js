import { UPDATE_MODAL } from "./actionTypes";

export default (
  state = {
    modal: null,
    fetch: false
  },
  action
) => {
  const { type, payload } = action;
  switch (type) {
    case UPDATE_MODAL:
      return { ...state, modal: payload };
    default:
      return state;
  }
};
