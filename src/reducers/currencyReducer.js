import {
  FETCH_CURRENCY,
  FETCH_CURRENCY_SUCCESS,
  UPDATE_BASE_VALUE,
  UPDATE_AVAILABLE_CURRENCIES,
  FETCH_CURRENCY_FAILED
} from "./actionTypes";

// default shown currencies rate
const available_currencies = [
  "USD",
  "CAD",
  "IDR",
  "GBP",
  "CHF",
  "SGD",
  "INR",
  "MYR",
  "JPY",
  "KRW"
];

export default (
  state = {
    data: { base: "", base_name: "", value: 0, rates: [], shows: [] },
    loading: true,
    error: null
  },
  action
) => {
  const { type, payload } = action;
  switch (type) {
    case FETCH_CURRENCY:
      return { ...state, loading: true };
    case FETCH_CURRENCY_SUCCESS:
      let { currencies, currencyRate, base, value } = payload;
      let rates = currencyRate.rates;
      let data = {
        base,
        base_name: "",
        value,
        rates: [],
        shows: []
      };

      data.base_name = currencies[base];

      // to get rates & shows
      // change rates object to array,
      // for get sorted selction input
      data.rates = Object.keys(rates).map(k => ({
        currency_code: k,
        currency: currencies[k],
        value: rates[k],
        show: available_currencies.includes(k)
      })).sort((a, b) => (a.currency_code < b.currency_code ? -1 : 1));

      // shown currency rate,
      // make it array, to available push/unshift it, so we can add the new one to the bottom/top
      data.shows = data.rates.filter(({ currency_code, show }) => currency_code !== base && show);

      return { ...state, data, loading: false, error: null };
    case UPDATE_BASE_VALUE:
      // handle non number value
      value = parseInt(payload);
      value = Number.isInteger(value) ? value : 0;

      return { ...state, data: { ...state.data, value } };
    case UPDATE_AVAILABLE_CURRENCIES:
      let rateIndex = state.data.rates.findIndex(
        ({ currency_code }) => currency_code === payload
      );
      let selected = state.data.rates[rateIndex];

      if (selected.show) {
        // remove shown currency
        state.data.shows.splice(
          state.data.shows.findIndex(
            ({ currency_code }) => currency_code === payload
          ),
          1
        );
      } else {
        // add shown currency
        state.data.shows.unshift({ ...selected, show: true });
      }

      // update currency rates
      state.data.rates[rateIndex].show = !selected.show;

      return {
        ...state,
        data: {
          ...state.data,
          rates: state.data.rates,
          shows: state.data.shows
        }
      };
    case FETCH_CURRENCY_FAILED:
      return {
        ...state,
        loading: false,
        error: payload
      };
    default:
      return state;
  }
};
