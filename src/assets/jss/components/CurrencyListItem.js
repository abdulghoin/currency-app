import { red } from "@material-ui/core/colors";

export default {
  container: {
    borderRadius: "8px",
    marginBottom: "10px",
    boxShadow:
      "0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12)",

    "&:last-child": {
      marginBottom: "0"
    }
  },

  textContainer: {
    width: "90%"
  },

  topSection: {
    fontSize: "1.1em",
    display: "flex",
    justifyContent: "space-between",
    marginBottom: "10px"
  },

  bottomSection: {
    "& *": {
      margin: "0 0 5px 0",

      "&:last-child": {
        marginBottom: "0"
      }
    }
  },

  scdAction: {
    color: red[400],
    cursor: "pointer"
  }
};
