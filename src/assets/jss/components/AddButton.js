import { blue } from "@material-ui/core/colors";

export default {
  fab: {
    position: "fixed",
    bottom: "8px",
    right: "8px",
    color: "#fff",
    backgroundColor: blue[400],

    "&:hover": {
      backgroundColor: blue[600]
    }
  }
};
