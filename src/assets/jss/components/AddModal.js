import { red, blue } from "@material-ui/core/colors";

export default theme => ({
  dialogWidth: {
    [theme.breakpoints.down("sm")]: {
      width: "90%"
    },

    [theme.breakpoints.up("md")]: {
      width: "50%"
    }
  },

  selectWidth: {
    width: "100%" 
  },

  cancelButton: {
    color: red[500]
  },

  nextButton: {
    color: blue[500]
  }
});
