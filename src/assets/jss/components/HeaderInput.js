export default {
  container: {
    backgroundColor: "#fff",
    zIndex: "1",
    width: "100%",
    position: "fixed",
    top: "0",
    left: "0",
    height: "15vh",
    boxShadow: "0 0 5px 2px grey",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",

    "& *": {
      margin: "0"
    }
  },

  content: {
    width: "90%"
  },

  headerText: {
    textAlign: "center",
    fontSize: "1em",
    marginBottom: "1%"
  },

  inputContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "1% 3%",
    border: "1.5px solid grey",
    borderRadius: "5px",

    "& *": {
      width: "10%"
    }
  },

  divider: {
    height: "20px",
    width: "2px"
  },

  input: {
    width: "80%",
    border: "none",
    outline: "none",
    textAlign: "right"
  }
};
