import { blue } from "@material-ui/core/colors";

export default {
  listPadding: {
    maxHeight: "100%",
    overflow: "auto",
    padding: "1.5vh 5%",

    "&::-webkit-scrollbar": {
      width: "0.5em"
    },

    "&::-webkit-scrollbar-thumb": {
      borderRadius: "0.5em",
      backgroundColor: blue[500]
    },

    "&::-webkit-scrollbar-track": {
      "-webkit-box-shadow": "inset 0 0 0.1em rgba(0,0,0,0.4)",
      borderRadius: "0.5em",
      backgroundColor: "#fff"
    }
  }
};
