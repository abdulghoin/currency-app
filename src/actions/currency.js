// action types
import {
  FETCH_CURRENCY,
  FETCH_CURRENCY_SUCCESS,
  UPDATE_BASE_VALUE,
  UPDATE_AVAILABLE_CURRENCIES,
  FETCH_CURRENCY_FAILED
} from "../reducers/actionTypes";

// xhr/fetch module
import axios from "axios";

// get currencies long name
function fetchCurrencies() {
  return axios.get("https://openexchangerates.org/api/currencies.json");
}

// get currency rates
function fetchCurrencyRates(base) {
  return axios.get("https://api.exchangeratesapi.io/latest", {
    params: {
      base
    }
  });
}

export const fetchCurrency = (base, value) => dispatch => {
  dispatch({ type: FETCH_CURRENCY });

  axios
    .all([fetchCurrencies(), fetchCurrencyRates(base)])
    .then(
      axios.spread((currencies, currencyRate) => {
        dispatch({
          type: FETCH_CURRENCY_SUCCESS,
          payload: {
            currencies: currencies.data,
            currencyRate: currencyRate.data,
            base,
            value
          }
        });
      })
    )
    .catch(err => {
      // console.log(err.response);
      dispatch({ type: FETCH_CURRENCY_FAILED, payload: err.response.data });
    });

  /* fetch('https://api.exchangeratesapi.io/latest?base=USD')
  .then(res => {
    console.log(res);
    let { 
      ok,
      status, 
      statusText
    } = res;
   // console.log(res.json());
    if (!ok) {
      dispatch({ type: FETCH_CURRENCY_FAILED, paylod: { status, statusText } });
    }
    return res.json();
  })
  .then(data => {
    dispatch({ type: FETCH_CURRENCY_SUCCESS, payload: data });
  })
  .catch(err => {
    console.log(err)
  }) */
};

export const updateBaseValue = value => dispatch =>
  dispatch({ type: UPDATE_BASE_VALUE, payload: value });

export const updateAvailableCurrencies = key => dispatch =>
  dispatch({ type: UPDATE_AVAILABLE_CURRENCIES, payload: key });
