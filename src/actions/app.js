import { UPDATE_MODAL } from "../reducers/actionTypes";

export const updateModal = modal => dispatch =>
  dispatch({ type: UPDATE_MODAL, payload: modal });
